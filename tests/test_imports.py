# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

"""Test file for imports."""


def test_package_import():
    """Test the import of the main package."""
    import gitlab_cibuildwheel_demo  # noqa: F401


def test_hello_world():
    from gitlab_cibuildwheel_demo.hello_world import hello_world

    assert hello_world() == "Hello world!"
