# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

"""Demo cython module."""

def hello_world():
    print("Hello World!")
    return "Hello world!"
