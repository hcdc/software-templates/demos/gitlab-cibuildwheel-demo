# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

image: python:3.9

include: "/ci/.gitlab-ci.build.yml"

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip

before_script:
  # replace git internal paths in order to use the CI_JOB_TOKEN
  - apt-get update -y && apt-get install -y pandoc graphviz
  - python -m pip install -U pip

test-package:
  stage: test
  script:
    - pip install twine
    - twine check dist/* wheelhouse/*
  needs:
    - job: build-package-sdist
    - job: build-package-linux
    - job: build-package-windows
      optional: true
    - job: build-package-macos

test:
  stage: test
  script:
    - make dev-install
    - make test
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  needs:
    - build-package-sdist

test-wheel-linux:
  # test the wheel on multiple operating systems
  stage: test
  image: docker:20.10
  variables:
    PYTHON: python
  before_script:
    - apk add --update --no-cache python3 py3-pip
    - python -m pip install -f ./wheelhouse/ gitlab-cibuildwheel-demo
  script:
    - python -P -c "from gitlab_cibuildwheel_demo import hello_world; hello_world.hello_world()"
  tags:
    - dind
  services:
    - docker:20.10-dind
  needs:
    - build-package-linux

test-wheel-macos:
  # test the wheel on multiple operating systems
  stage: test
  image: hcr.helmholtz.cloud/ci/macos/ventura-base:latest
  variables:
    PYTHON: python3
  before_script:
    - brew install python@3.10
    - brew link --overwrite python@3.10
    - python3 -m pip install -f ./wheelhouse/ gitlab-cibuildwheel-demo
  script:
    - python3 -P -c "from gitlab_cibuildwheel_demo import hello_world; hello_world.hello_world()"
  tags:
    - macos-medium-m1
  needs:
    - build-package-macos

test-wheel-windows:
  # test the wheel on multiple operating systems
  stage: test
  image: winamd64/python:3
  before_script:
    - python -m pip install -f wheelhouse\ gitlab-cibuildwheel-demo
  script:
    - python -P -c "from gitlab_cibuildwheel_demo import hello_world; hello_world.hello_world()"
  variables:
    PYTHON: python
  tags:
    - windows-docker
  needs:
    - build-package-windows

test-docs:
  stage: test
  script:
    - make dev-install
    - make -C docs html
    - make -C docs linkcheck
  artifacts:
    paths:
    - docs/_build
  needs:
    - build-package-sdist


deploy-package:
  stage: deploy
  needs:
    - job: build-package-sdist
    - job: build-package-linux
    - job: build-package-windows
    - job: build-package-macos
    - job: test-package
    - job: test-docs
    - job: test
  only:
    - main
  script:
    - pip install twine
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/* wheelhouse/*
