# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

"""pytest configuration script for gitlab-cibuildwheel-demo."""

import pytest  # noqa: F401
