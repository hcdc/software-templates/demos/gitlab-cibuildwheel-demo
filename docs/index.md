<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

<!--
gitlab-cibuildwheel-demo documentation master file
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.
-->

# Welcome to gitlab-cibuildwheel-demo's documentation!

[![CI](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/-/graphs/main/charts)
<!-- TODO: uncomment the following line when the package is registered at https://readthedocs.org -->
<!-- [![Docs](https://readthedocs.org/projects/gitlab-cibuildwheel-demo/badge/?version=latest)](https://gitlab-cibuildwheel-demo.readthedocs.io/en/latest/) -->
[![Latest Release](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/-/badges/release.svg)](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo)
<!-- TODO: uncomment the following line when the package is published at https://pypi.org -->
<!-- [![PyPI version](https://img.shields.io/pypi/v/gitlab-cibuildwheel-demo.svg)](https://pypi.python.org/pypi/gitlab-cibuildwheel-demo/) -->
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
<!-- TODO: uncomment the following line when the package is registered at https://api.reuse.software -->
<!-- [![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo)](https://api.reuse.software/info/codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo) -->

**Demo Package for cibuildwheel on Gitlab CI**

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of gitlab-cibuildwheel-demo! Stay tuned for
updates and discuss with us at <https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo>
```

```{toctree}
---
maxdepth: 2
caption: "Contents:"
---
installation
api
contributing
```

## How to cite this software

```{eval-rst}
.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff
```

# License information

Copyright © 2024 Helmholtz-Zentrum hereon GmbH

The source code of gitlab-cibuildwheel-demo is licensed under MIT.

If not stated otherwise, the contents of this documentation is licensed
under CC-BY-4.0.

## Indices and tables

-   {ref}`genindex`
-   {ref}`modindex`
-   {ref}`search`
