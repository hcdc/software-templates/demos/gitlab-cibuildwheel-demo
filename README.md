<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

# Demo Package for cibuildwheel on Gitlab CI

[![CI](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/-/graphs/main/charts)
<!-- TODO: uncomment the following line when the package is registered at https://readthedocs.org -->
<!-- [![Docs](https://readthedocs.org/projects/gitlab-cibuildwheel-demo/badge/?version=latest)](https://gitlab-cibuildwheel-demo.readthedocs.io/en/latest/) -->
[![Latest Release](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo/-/badges/release.svg)](https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo)
<!-- TODO: uncomment the following line when the package is published at https://pypi.org -->
<!-- [![PyPI version](https://img.shields.io/pypi/v/gitlab-cibuildwheel-demo.svg)](https://pypi.python.org/pypi/gitlab-cibuildwheel-demo/) -->
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
<!-- TODO: uncomment the following line when the package is registered at https://api.reuse.software -->
<!-- [![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo)](https://api.reuse.software/info/codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo) -->


## Preface

This is a demo package to test the building of a python package on multiple
operating systems using `cibuildwheel`. It only implements a small `Cython`
setup that you can find in
[gitlab_cibuildwheel_demo/hello_world.pyx](gitlab_cibuildwheel_demo/hello_world.pyx)
and builds on linux, windows and macos.

The config for the builds in the CI can be found in the file
[ci/.gitlab-ci.build.yml](ci/.gitlab-ci.build.yml). For information on the
gitlab runners we are using here, please refer to the docs for the HIFIS
gitlab at https://hifis.net/doc/software/gitlab/ci/.

## Installation

### Installation via pre-compiled wheels

Install this package in a dedicated python environment via

```bash
python -m venv venv
source venv/bin/activate
pip install --extra-index-url https://codebase.helmholtz.cloud/api/v4/projects/11473/packages/pypi/simple gitlab-cibuildwheel-demo
```

This will download the pre-compiled wheels for your specific operating system.
To verify that everything works, you can run

```bash
python -c "from gitlab_cibuildwheel_demo import hello_world; hello_world.hello_world()"
```

### Installation from git

To use this in a development setup, clone the [source code][source code] from
gitlab, start the development server and make your changes::

```bash
git clone https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo
cd gitlab-cibuildwheel-demo
python -m venv venv
source venv/bin/activate
make dev-install
```

This will install Cython and compile the code locally on your computer.

[source code]: https://codebase.helmholtz.cloud/hcdc/software-templates/demos/gitlab-cibuildwheel-demo

## Technical note

This package has been generated from the template
https://codebase.helmholtz.cloud/hcdc/software-templates/python-package-template.git.

See the template repository for instructions on how to update the skeleton for
this package.


## License information

Copyright © 2024 Helmholtz-Zentrum hereon GmbH



Code files in this repository are licensed under the
MIT, if not stated otherwise
in the file.

Documentation files in this repository are licensed under CC-BY-4.0, if not stated otherwise in the file.

Supplementary and configuration files in this repository are licensed
under CC0-1.0, if not stated otherwise
in the file.

Please check the header of the individual files for more detailed
information.



### License management

License management is handled with [``reuse``](https://reuse.readthedocs.io/).
If you have any questions on this, please have a look into the
[contributing guide][contributing] or contact the maintainers of
`gitlab-cibuildwheel-demo`.

[contributing]: https://gitlab-cibuildwheel-demo.readthedocs.io/en/latest/contributing.html
